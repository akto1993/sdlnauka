/*
Przykładowy program na zajęcia z SGD, można go wykorzystywać do dowolnych celów,
proszę zachować jedynie notatkę o autorze
Tadeusz Puźniakowski
*/

#include <SDL.h>
#include <vector>
#include <tuple>
#include <map>
#include <cmath>
#include <functional>
#include <chrono>
#include <iostream>
#include <thread>
#include <stdexcept>
#include "SDL/SDL_ttf.h"

#include <spriteengine.hpp>

using namespace std;

// z dokumentacji SDL
Uint32 getpixel(SDL_Surface *surface, int x, int y) {
    int bpp = surface->format->BytesPerPixel;
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;
    switch(bpp) {
    case 1: return *p; break;
    case 2: return *(Uint16 *)p; break;
    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2];
        else
            return p[0] | p[1] << 8 | p[2] << 16;
        break;
    case 4: return *(Uint32 *)p; break;
    default: return 0;
    }
}


tuple < SDL_Surface *, SDL_Texture * > SpriteEngine::getResource(const string &fname) {
	if (cache.count(fname) == 0) {
		SDL_Surface *s = SDL_LoadBMP( fname.c_str() );
		if( s == NULL ) { 
			throw std::runtime_error(SDL_GetError());
		}
		SDL_SetColorKey( s, SDL_TRUE, SDL_MapRGB( s->format, 0, 0xFF, 0xFF ) );
		SDL_Texture *t = SDL_CreateTextureFromSurface(renderer, s);
		cache[fname] = make_tuple(s,t);
	}
	cacheRef[fname] = cacheRef[fname] + 1;
	std::cout << "new " << fname << "\n";
	return cache[fname];
}

void SpriteEngine::drawParticle(double x , double y) {
	double visScale = 20;
	
	SDL_Rect dstrect;

	auto pp = toScreenCoord({x,y});
	dstrect = { pp[0]+(unitSize/2), pp[1]+unitSize, visScale * 0.3,visScale * 0.3 };

	SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);

	SDL_RenderFillRect(renderer,&dstrect);
}

void SpriteEngine::drawPoints(int playerPotints, int player2Points){
	/*TTF_Font *Sans = TTF_OpenFont("Sans.ttf", 24); //this opens a font style and sets a size
	std::cout<<"lol\n";
	SDL_Color White = {0, 0, 0};  // this is the color in rgb format, maxing out all would give you the color white, and it will be your text's color

	SDL_Surface *surfaceMessage = TTF_RenderText_Solid(Sans, "put your text here", White); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first

	SDL_Texture *Message = SDL_CreateTextureFromSurface(renderer, surfaceMessage); //now you can convert it into a texture

	SDL_Rect Message_rect; //create a rect
	Message_rect.x = 122;  //controls the rect's x coordinate 
	Message_rect.y = 122; // controls the rect's y coordinte
	Message_rect.w = 100; // controls the width of the rect
	Message_rect.h = 100; // controls the height of the rect

	//Mind you that (0,0) is on the top left of the window/screen, think a rect as the text's box, that way it would be very simple to understance

	//Now since it's a texture, you have to put RenderCopy in your game loop area, the area where the whole code executes

	SDL_RenderCopy(renderer, Message, NULL, &Message_rect); //you put the renderer's name first, the Message, the crop size(you can ignore this if you don't want to dabble with cropping), and the rect which is the size and coordinate of your texture
	*/
	double visScale = 120;
	
	SDL_Rect dstrect;

	if (playerPotints > player2Points){
		//player1 win
		//std::cout<<"player1 win "<< playerPotints << "player2 "<< player2Points <<" \n";

		auto pp = toScreenCoord({1,17});
		dstrect = { pp[0]+(unitSize/2), pp[1]+unitSize, visScale * 0.3,visScale * 0.3 };

		SDL_SetRenderDrawColor(renderer, 0,255, 0, 255);

		SDL_RenderFillRect(renderer,&dstrect);
	}else if(playerPotints < player2Points){
		//player2 win
		//std::cout<<"player2 win "<< player2Points << "player "<< playerPotints <<" \n";
		auto pp = toScreenCoord({21,17});
		dstrect = { pp[0]+(unitSize/2), pp[1]+unitSize, visScale * 0.3,visScale * 0.3 };

		SDL_SetRenderDrawColor(renderer, 0,255, 0, 255);

		SDL_RenderFillRect(renderer,&dstrect);
	}else{
		//draw
		//std::cout<<"draw player1 "<< playerPotints << "player2 "<< player2Points <<" \n";
		auto pp = toScreenCoord({11,17});
		dstrect = { pp[0]+(unitSize/2), pp[1]+unitSize, visScale * 0.3,visScale * 0.3 };

		SDL_SetRenderDrawColor(renderer, 0,255, 0, 255);

		SDL_RenderFillRect(renderer,&dstrect);
	}
}

void SpriteEngine::freeResource(const string &fname) {
	cacheRef[fname] = cacheRef[fname] - 1;
	if (cacheRef[fname] <= 0) {
		SDL_Surface *s; SDL_Texture *t;
		std::cout << "delete " << fname << "\n";
		std::tie(s, t) = cache[fname];
		SDL_DestroyTexture(t);
		SDL_FreeSurface(s);
		cacheRef.erase(fname);
		cache.erase(fname);
	}
}

void SpriteEngine::startFrame() {
	SDL_SetRenderDrawColor(renderer, 0, 0, 64, 0);
	SDL_RenderClear(renderer);
}

void SpriteEngine::finishFrame() {
	SDL_RenderPresent(renderer);
}

//Współrzędne gey na współrzędne ekranu
vector < int > SpriteEngine::toScreenCoord(const vector < double > &p) {
	return {(int)((p[0]-cameraPosition[0])*unitSize+screenSize[0]/2),  
		(int)((-p[1]+cameraPosition[1])*unitSize+screenSize[1]/2)};
}

SpriteEngine::SpriteEngine() {
	SDL_Init( SDL_INIT_EVERYTHING | SDL_INIT_JOYSTICK);

	screenSize = {800,600};
	window = SDL_CreateWindow("Gra na SGD",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screenSize[0], screenSize[1], 0);
	if(TTF_Init() < 0) {
		string i = TTF_GetError();
		std::cout <<"TTF_Init nie działa "<< TTF_Init() <<std::endl;
	}else{
		std::cout <<"TTF_Init nie działa \n";
	}

	renderer = SDL_CreateRenderer(window, -1, 0);
	cameraPosition = {0,0};
	cameraPosition2 = {0,0};
}

SpriteEngine::~SpriteEngine() {
	SDL_DestroyWindow( window );
	SDL_Quit(); 
}

SpriteEngine spriteEngine;
