/*
Przykładowy program na zajęcia z SGD, można go wykorzystywać do dowolnych celów,
proszę zachować jedynie notatkę o autorze
Tadeusz Puźniakowski
*/

#include <SDL.h>

#include <vector>
#include <tuple>
#include <map>
#include <cmath>
#include <functional>
#include <chrono>
#include <iostream>
#include <thread>
#include <stdexcept>
	
	/*
	+---------------+
	|               |
	|               |
	|0,0            |
	+---------------+
	kafelki rozmiaru 32x32
	*/


#include <spriteengine.hpp>
#include <gamemap.hpp>
#include <player.hpp>

#include <veclib/veclib.hpp>


using std::vector;
using std::map;
using std::tuple;
using std::make_tuple;
using std::function;
using std::string;
using std::this_thread::sleep_for;
using std::chrono::milliseconds;
using std::chrono::microseconds;
using std::chrono::system_clock;

const std::vector < double > gravityy = {0,-10};

class Particle {
	public:
		vector < double > position;
		vector < double > velocity;
		double ttl;
		/*
		Particle(double x, double y, double dx, double dy):
				position({x,y}), velocity({dx,dy}) {
					ttl = 10;
					pt = BULLET;
		};*/
		Particle(vector < double > position0, vector < double > velocity0):
				position(position0), velocity(velocity0) {
					ttl = 1;
		};
};

class Game {
	public:
	
	SDL_Surface * s_background;
	SDL_Texture * t_background;
	SDL_Joystick *controller = nullptr;
	SDL_Joystick *controller2 = nullptr;
	SDL_Event ev;
	SDL_Event ev2;
	int JOYSTICK_DEAD_ZONE = 8000;
	int xDir = 0;
	int xDir2 = 0;

	GameMap gameMap;
	Player player;
	double playerShooted = 0;
	Player player2;
	double player2Shooted = 0;
	vector < Particle > particles;
	
	void draw(double t);
	int input();
	void physics(double dt);
	Game(const map<string,string> config = map<string,string>());
	
	virtual ~Game() ;
};

void Game::draw(double t) {
	spriteEngine.startFrame();
	
	SDL_Rect dstrect = {
		(int)(-spriteEngine.cameraPosition[0]*unitSize-spriteEngine.screenSize[0])>>1,  
		(int)(spriteEngine.screenSize[1] + spriteEngine.cameraPosition[1]*unitSize-spriteEngine.screenSize[1]*3)>>1,
		(int)spriteEngine.screenSize[0]*4,(int)spriteEngine.screenSize[1]*4};
	SDL_RenderCopy(spriteEngine.renderer, t_background, NULL, &dstrect);
	
	gameMap.draw(t);		
	player.draw(t);
	player2.draw(t);
	spriteEngine.drawPoints(player.points,player2.points);
	//wyświetlanie hitboxa
	//spriteEngine.drawParticle(player.position[0]-0.5,player.position[1]-0.8);
	//spriteEngine.drawParticle(player.position[0]+0.3,player.position[1]+1);

	for (auto p: particles) {
			spriteEngine.drawParticle(p.position[0],p.position[1]);
		}

	spriteEngine.finishFrame();
};

int Game::input() {
	int ret = 0;
	SDL_Event e;
	while( SDL_PollEvent( &e ) != 0 ) { 
		if( e.type == SDL_QUIT ) { 
			ret |= 1;
		} else if (e.type == SDL_KEYDOWN) {
			if (e.key.keysym.sym == SDLK_ESCAPE) ret |= 1;
		} else if (e.type == SDL_KEYUP) {
			//if (keyUpHandlers.count(e.key.keysym.sym) > 0) keyUpHandlers[e.key.keysym.sym](e);
		}
	}
	
	// sterowanie
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	player.acceleration = {0,0};
	player2.acceleration = {0,0};
	//std::cout << "player2 position " << player2.position << "\n";
	if( e.type == SDL_JOYAXISMOTION )
		{
			//Motion on controller 0
			if( e.jaxis.which == 0 )
			{                        
				//X axis motion
				if( e.jaxis.axis == 0 )
				{
					//Left of dead zone
					if( e.jaxis.value < -JOYSTICK_DEAD_ZONE )
					{
						xDir = -1;
					}
					//Right of dead zone
					else if( e.jaxis.value > JOYSTICK_DEAD_ZONE )
					{
						xDir =  1;
					}
					else
					{
						xDir = 0;
					}
				}
			}
		}
	//sterowanie gracza pierwszego
	if (player.isOnGround(gameMap)) {
		if (state[SDL_SCANCODE_LEFT] || xDir == -1)   player.acceleration[0] -= 20.0;
		if (state[SDL_SCANCODE_RIGHT] || xDir == 1)  player.acceleration[0] += 20.0;
		if (state[SDL_SCANCODE_UP] || SDL_JoystickGetButton(controller,2))     player.speed[1] = 9.0;
		if (playerShooted <= 0)  {
			if (state[SDL_SCANCODE_KP_3] || SDL_JoystickGetButton(controller,1)){
				particles.push_back(Particle({player.position[0]+1,player.position[1]},{15,0}));
				playerShooted = 0.5;
			}
		}
		if (playerShooted <= 0)  {
			if (state[SDL_SCANCODE_KP_2] || SDL_JoystickGetButton(controller,3)){
				particles.push_back(Particle({player.position[0]-1,player.position[1]},{-15,0}));
				playerShooted = 0.5;
			}
		}
	} else {
		if (state[SDL_SCANCODE_LEFT] || xDir == -1)   player.acceleration[0] -= 5.0;
		if (state[SDL_SCANCODE_RIGHT] || xDir == 1)  player.acceleration[0] += 5.0;
		if (playerShooted <= 0)  {
			if (state[SDL_SCANCODE_KP_3] || SDL_JoystickGetButton(controller,1)){
				particles.push_back(Particle({player.position[0]+1,player.position[1]},{15,0}));
				playerShooted = 0.5;
			}
		}
		if (playerShooted <= 0)  {
			if (state[SDL_SCANCODE_KP_2] || SDL_JoystickGetButton(controller,3)){
				particles.push_back(Particle({player.position[0]-1,player.position[1]},{-15,0}));
				playerShooted = 0.5;
			}
		}
	}
	//sterowanie gracza drugiego
	if (player2.isOnGround(gameMap)) {
		if (state[SDL_SCANCODE_A])	player2.acceleration[0] -= 20.0;
		if (state[SDL_SCANCODE_D])	player2.acceleration[0] += 20.0;
		if (state[SDL_SCANCODE_W])	player2.speed[1] = 9.0;    
		if (state[SDL_SCANCODE_B] && player2Shooted <= 0)  {
			particles.push_back(Particle({player2.position[0]+1,player2.position[1]},{15,0}));
			player2Shooted =0.5;
			}
		if (state[SDL_SCANCODE_V] && player2Shooted <= 0)  {
			particles.push_back(Particle({player2.position[0]-1,player2.position[1]},{-15,0}));
			player2Shooted =0.5;
			}
	} else {
		if (state[SDL_SCANCODE_A])  player2.acceleration[0] -= 5.0;
		if (state[SDL_SCANCODE_D])  player2.acceleration[0] += 5.0;
		if (state[SDL_SCANCODE_B] && player2Shooted <= 0)  {
			particles.push_back(Particle({player2.position[0]+1,player2.position[1]},{15,0}));
			player2Shooted = 0.5;
			}
		if (state[SDL_SCANCODE_V] && player2Shooted <= 0)  {
			particles.push_back(Particle({player2.position[0]-1,player2.position[1]},{-15,0}));
			player2Shooted = 0.5;
			}
	}
	return ret;
};

void Game::physics(double dt) {
	player.physics(gameMap, dt);
	playerShooted -= dt;
	player2.physics(gameMap, dt);
	player2Shooted -= dt;

	for (int i = 0; i < particles.size(); i++) {
		particles[i].velocity = particles[i].velocity + gravityy*dt;
		particles[i].position = particles[i].position + particles[i].velocity*dt;
		particles[i].ttl -= dt;
		if (particles[i].position[0] >= player2.position[0]-0.5 && particles[i].position[0] <= player2.position[0]+0.3 && particles[i].position[1] >= player2.position[1]-0.8 && particles[i].position[1] <= player2.position[1]+1){
			//std::cout<<"trafiles gracza 2\n";
			player.points += 1;
			player2.position = {23,2};
			player2.speed = {0,0};
			player2.acceleration = {0,0};
			particles[i] = particles.back();
				particles.pop_back();
		}
		if (particles[i].position[0] >= player.position[0]-0.5 && particles[i].position[0] <= player.position[0]+0.3 && particles[i].position[1] >= player.position[1]-0.8 && particles[i].position[1] <= player.position[1]+1){
			//std::cout<<"trafiles gracza 1\n";
			player2.points += 1;
			player.position = {0,2};
			player.speed = {0,0};
			player.acceleration = {0,0};
			particles[i] = particles.back();
				particles.pop_back();
		}
		//std::cout<<particles[i].position[0] << " " <<  player2.position[0] << "\n";
		if (particles[i].ttl <=0){
			particles[i] = particles.back();
				particles.pop_back();
		}
		if (gameMap.getCollision({particles[i].position[0]+0.5,particles[i].position[1]} ) != 0){
			particles[i] = particles.back();
				particles.pop_back();
		}
	}
};

Game::Game(const map<string,string> config) {
	std::tie(s_background, t_background) = spriteEngine.getResource("img/background.bmp");
	gameMap = GameMap("map.bmp");
};

Game::~Game() {
};





int main( int argc, char* args[] ) { 
	Game game;
	/*for(int i = 0 ; i <SDL_NumJoysticks(); i++){*/
	//std::cout<<SDL_NumJoysticks()<<std::endl;
	if(SDL_NumJoysticks() >=1 ){
		game.controller = SDL_JoystickOpen(0);
		std::cout<< "kontroler "<< SDL_JoystickName(game.controller)<<"\n";
		//break;
	}
	if(SDL_NumJoysticks() >=2 ){
		game.controller2 = SDL_JoystickOpen(1);
		std::cout<< "kontroler2 "<< SDL_JoystickName(game.controller)<<"\n";
		//break;
	}
	//}
	game.player2.position = {23,2};
	spriteEngine.cameraPosition = {12,8};
	bool finishCondition = false;
    
    auto prevTime = std::chrono::system_clock::now();
    auto t0 = prevTime; // zero time
    unsigned long int frame = 0;
    double t = 0;
	while (!finishCondition) {
		double dt=0;
		std::chrono::duration<double> cdt; // frame time
		game.draw(t);
		auto now = system_clock::now();
		cdt = now - prevTime; // przyrost czasu w sekundach
		dt = cdt.count();
		prevTime = now;
		// std::cout << "FPS: " << (1.0/cdt.count()) << std::endl;
		if (game.input() != 0) finishCondition = true;
		game.physics(dt);
		t+= dt;
	}
	return 0; 
}
