/*
Przykładowy program na zajęcia z SGD, można go wykorzystywać do dowolnych celów,
proszę zachować jedynie notatkę o autorze
Tadeusz Puźniakowski
*/

#ifndef __SPRITEENGINE__
#define __SPRITEENGINE__

#include <SDL.h>
#include <string>
#include <map>
#include <vector>
#include "SDL/SDL_ttf.h"
#include <veclib/veclib.hpp>


const double unitSize = 32;
const std::vector < double > gravity = {0,-10};



// z dokumentacji SDL
Uint32 getpixel(SDL_Surface *surface, int x, int y);


class SpriteEngine {
	protected:
		std::map < std::string, std::tuple < SDL_Surface *, SDL_Texture * > > cache;
		std::map < std::string, int > cacheRef;
	public:

		SDL_Window *window;
		SDL_Renderer *renderer;
		TTF_Font *Sans;

		std::vector < double > screenSize;
		std::vector < double > cameraPosition;
		std::vector < double > cameraSpeed;
		std::vector < double > cameraPosition2;
		std::vector < double > cameraSpeed2;
		const std::vector < double > gravity;
		
		std::tuple < SDL_Surface *, SDL_Texture * > getResource(const std::string &fname);
		
		void freeResource(const std::string &fname);

		void drawParticle(double x , double y);
		void drawPoints(int playerPotints, int player2Points);

		void startFrame();
	
		void finishFrame() ;

		std::vector < int > toScreenCoord(const std::vector < double > &p) ;
		
		std::vector < int > toScreenCoord2(const std::vector < double > &p) ;

		SpriteEngine() ;
		
		~SpriteEngine();
};

extern SpriteEngine spriteEngine;


#endif
